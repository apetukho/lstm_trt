# TRT LSTM Code for Particle Identification
# ATLAS Qualification Task
# Created by: Arif Bayirli
# Last modified: 03.10.2019

from helper_functions import * #see helper_functions.py

#Define the training/test/validation set sizes
test_size = 3000
val_size = 3000
train_size = 150000
np.random.seed(111)
prob = 0.5 #balance of the train/test/val data sets (0.5 = balanced)

#Define the dictionary of the electron and muon MC files
e_train_files = "/hdd/cernbox/NtuplerJobs/trtLSTM/e_train_files"
e_test_files = "/hdd/cernbox/NtuplerJobs/trtLSTM/e_test_files"
m_train_files = "/hdd/cernbox/NtuplerJobs/trtLSTM/m_train_files"
m_test_files = "/hdd/cernbox/NtuplerJobs/trtLSTM/m_test_files"

filelist_train = {"electron": findFiles(e_train_files + '/*.root'), "muon": findFiles(m_train_files+ '/*.root')}
filelist_test = {"electron": findFiles(e_test_files + '/*.root'), "muon": findFiles(m_test_files + '/*.root')}

#Initialize the generators to be used to create test and validation sets
electron_val = trackGenerator(filelist_test, particle_type = "electron")
muon_val = trackGenerator(filelist_test, particle_type = "muon")

#Create test set
test_set = []
val_set = []
ytest = []
test_eProbHT = []
for k in range(test_size):
    particle = next(electron_val) if np.random.random() < prob else next(muon_val)
    test_set.append(particle)
    ytest.append(particle[3])
    test_eProbHT.append(particle[2])
print("Test data created!")

#Create validation set
yval = []
val_eProbHT = []
for k in range(val_size):
    particle = next(electron_val) if np.random.random() < prob else next(muon_val)
    val_set.append(particle)
    yval.append(particle[3])
    val_eProbHT.append(particle[2])
print("Validation data created!")

#define Pytorch LSTM Model Clas
class Model(nn.Module):

    def __init__(self):
        super(Model, self).__init__()
        self.lstm = nn.LSTM(input_size=input_size,
                            hidden_size=hidden_size, num_layers=num_layers, 
                            dropout = dropout, batch_first=True)
        self.hidden2label = nn.Linear(hidden_size + num_track_vars, num_classes) 
        self.sigmoid = torch.nn.Sigmoid()
        
    def forward(self, x, track_var, hidden):
        # Reshape input (batch first)
        sequence_length = batch_size
        
        x = x.view(batch_size, sequence_length, input_size)
        # Propagate input through RNN
        # Input: (batch, seq_len, input_size)
        # hidden: (num_layers * num_directions, batch, hidden_size)
        out, hidden = self.lstm(x, hidden)
        last_out = torch.cat((track_var, out), 2)
        class_score = self.hidden2label(last_out)
        return self.sigmoid(class_score), hidden

    def init_hidden(self):
        # Initialize hidden and cell states
        # (num_layers * num_directions, batch, hidden_size)
        return (torch.randn(num_layers, 1, hidden_size),
                torch.randn(num_layers, 1, hidden_size))


def train(label_tensor, track_tensor, track_var):
    """Train function that takes a given track and label
    and updates the parameters of the model
    It loops over the hits in the track and feeds them to the model
    one by one. For each track it returns the output and corresponding loss
    """
    hidden = model.init_hidden()
    optimizer.zero_grad()

    for i in range(track_tensor.size()[0]):
        output, hidden = model(track_tensor[i], track_var, hidden)
        
    loss = criterion(output, label_tensor)
    loss.backward()
    optimizer.step()
    
    return output, loss.item()


def evaluate(label_tensor, track_tensor, track_var):
    """Evaluates a given track tensor (hit + track variables) and label tensor
    This is a helper function that is used in the "predict" method below
    and returns probability and the loss
    """
    hidden = model.init_hidden()

    for i in range(track_tensor.size()[0]):
        output, hidden = model(track_tensor[i], track_var, hidden)
    loss = criterion(output, label_tensor)
    
    return output, loss.item()

def predict(track, track_var, test_label):
    """Gives the probability of a track given a label
    returns the probability and loss
    parameters:
    track: list of hit variables for each hit on a track (list of lists)
    track_var: track variables to be concatenated with LSTM output
    test_label: 1 for electron , 0 for muon
    
    returns:
    output = (float) probability of being an electron
    loss = (float) loss value
    """
    with torch.no_grad():
            track_var = torch.FloatTensor([[track_var]])
            track_tensor = torch.autograd.Variable(torch.FloatTensor(track))
            label_tensor = torch.FloatTensor([[[test_label]]])
            output, loss = evaluate(label_tensor, track_tensor, track_var)
            
    return output, loss

# Instantiate RNN model with following input variables
input_size = 16 #number of hit variables (input to LSTM layer)
num_track_vars = 8 #number of track variables (concatanated with output of LSTM)
hidden_size = 50 #hidden dimension of LSTM
num_layers = 1 #number of LSTM layers
batch_size = 1 #number of batches
num_classes = 1 #output of overall model - since we get only probability it is 1
dropout = 0

model = Model()
print(model)

# Set Binary Cross Entropy loss and Adagarad optimizer function with learning rate
criterion = nn.BCELoss()
optimizer = torch.optim.Adagrad(model.parameters(), lr=0.1)

plot_loss = True #Store the loss values if True
state_save = True #Save the model parameters if True

all_losses = []
train_loss_list = []
test_loss_list = []
val_loss_list =[]

numEpochs = 2 #number of Epochs

f = open('OUTPUT_Training.dat','w')
f.write("-----Training Output------ \n")
f.write("-----Full Hits + Track Vars - CPU results------\n")
f.write("Number of data points: {} \n".format(train_size))
f.write("Number of epochs: {} \n".format(numEpochs))

train_time = time.time()
for epoch in range(numEpochs):
    train_loss = 0
    print("Epoch: ", epoch + 1)
    electron_train = trackGenerator(filelist_train, particle_type = "electron")
    muon_train = trackGenerator(filelist_train, particle_type = "muon")
    
    start = time.time()
    counter = 0
    for k in range(train_size):
        counter += 1
        #Comment out the counter for less overhead
        sys.stdout.write("Training - progress: %d%%   \r" % (float(counter)*100./train_size) )
        sys.stdout.flush()
        
        particle = next(electron_train) if np.random.random() < prob else next(muon_train)
        tick = time.time()
        track_var = torch.FloatTensor([[particle[1]]])
        track_tensor = torch.autograd.Variable(torch.FloatTensor(particle[0]))
        label_tensor = torch.FloatTensor([[[particle[3]]]])
        output, loss = train(label_tensor, track_tensor, track_var)
        train_loss += loss
    
    train_loss = train_loss/train_size
    train_loss_list.append(train_loss)       
    print("Training finished for epoch: ", epoch + 1)
    print("Time for training: ", timeSince(start))
    
    #Evaluate on the test set
    test_loss = 0
    y_scores = []

    for k in range(test_size):
        output, loss = predict(test_set[k][0], test_set[k][1], test_set[k][3])
        y_scores.append(output)
        test_loss += loss
    test_loss = test_loss/test_size
    test_loss_list.append(test_loss)   
    
    #Evaluate on the validation set
    val_loss = 0
    val_scores = []

    for k in range(val_size):
        output, loss = predict(val_set[k][0], val_set[k][1],  val_set[k][3])
        val_scores.append(output)
        val_loss += loss
    val_loss = val_loss/val_size
    val_loss_list.append(val_loss)
    
    print("Epoch {} Training - Validation - Test  Losses: {} - {} - {}".format(epoch+1, train_loss, val_loss, test_loss))
    
    test_auc = roc_auc_score(ytest, y_scores)
    eProbHT_auc = roc_auc_score(ytest, test_eProbHT)
    print("LSTM AUC: ", test_auc)
    print("eProbHT AUC: ", roc_auc_score(ytest, test_eProbHT))
    fpr, tpr, thresholds = roc_curve(ytest, y_scores)
    print("Epoch: {}: Test AUC: {}".format(epoch+1, test_auc))

    plt.figure()
    plot_roc_curve(fpr, tpr, label = "LSTM (hits) ({})".format(np.round(test_auc,4)))
    fpr, tpr, thresholds = roc_curve(ytest, test_eProbHT)
    plot_roc_curve(fpr,tpr, label = "eProbHT ({})".format(np.round(eProbHT_auc,4)))
    plt.title("Pytorch LSTM 16Hit + TrackVars - {} epochs".format(epoch + 1))
    plt.legend()
    plt.savefig("ROC_LSTM_Hits_TrackVars_{}data_epoch{}".format(train_size, epoch + 1))
    #plt.show()    

    f.write("Epoch {} Training - Validation - Test  Losses: {} - {} - {}\n".format(epoch+1, train_loss, val_loss, test_loss))
    f.write("Epoch {} - eProbHT AUC: {} / LSTM AUC: {}\n".format(epoch+1, np.round(eProbHT_auc,5), np.round(test_auc,5)))
    #Write the state to a file
    if(state_save == True):
        state = {'epoch': epoch+1,
                 'state_dict': model.state_dict(),
                 'optimizer': optimizer.state_dict(),
                 'comment: ': 'CPU_{} epoch {} data - All 16 hits + Track vars - Adagrad lr = 0.1'.format(numEpochs, train_size)}
        filename = 'All_16Hits_TrackVars_CPU_epoch{}.pth'.format(epoch+1)
        torch.save(state, filename)
f.write("Total time for training: {} \n".format(timeSince(train_time)))
f.close()

print("Total training time: ", timeSince(train_time))

epoch_list = np.linspace(0, epoch, epoch+1)+1 #define the rane for epochs for the plot

plt.figure()
plt.plot(epoch_list, train_loss_list, label = "Training")
plt.plot(epoch_list, test_loss_list, label = "Test")
plt.plot(epoch_list, val_loss_list, label = "Validation")
plt.xlabel("Epoch")
plt.ylabel("Loss")
plt.legend()
plt.savefig("Train-Val-Test_Loss{}".format(epoch + 1))
#plt.show()



