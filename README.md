# lstm_trt

Sample (CPU) code for LSTM model training in Pytorch (version 1.0.1) for the TRT Particle Identification Task.

It uses Ntuples created by [TRTNTupler](https://gitlab.cern.ch/ddavis/TRTNTupler) created by Douglas Raymond Davis and trains an LSTM Model with the hit + track information.

The import statements, helper functions, track generator and variable statistics are included in the `helper_functions.py`.
It requires *[uproot](https://github.com/scikit-hep/uproot)* package from [scikit-hep](https://github.com/scikit-hep) library.

The training and test (absolute) file paths needs to be modified in the main function (`lstm_train_test.py`)

In order to run the code:

`python3 lstm_train_test.py`

This is a sample, "toy" code for demonstration purposes.

Arif Bayirli

(TRT SW Group)