# TRT LSTM Code Helper Functions
# Import statements, helper functions and variables statistics
# The statistics have been calculated by going over all the hits for a given track
# Track generator is also included
# Created by: Arif Bayirli
# Last modified: 03.10.2019

import numpy as np
import uproot
import matplotlib.pyplot as plt

from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, roc_auc_score
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim

import sys
import glob
import math
import time
import os

def findFiles(path): 
    """Finds all of the file in a directory whose path is given
    """
    return glob.glob(path)

def plot_roc_curve(fpr, tpr, label = None):
    plt.plot(fpr, tpr, linewidth = 2, label= label)
    plt.plot([0, 1], [0,1], 'k--')
    plt.axis([0,1,0,1])
    plt.xlabel("False Positive Rate")
    plt.ylabel("True Positive Rate")

def timeSince(since):
    now = time.time()
    s = now - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)


#Hit variables
mean_HTMB= 0.18718335942204678; std_HTMB= 0.39005864863828316
mean_gasType= 0.4232464962422897; std_gasType= 0.494073779572358
mean_drifttime= 22.226789474487305; std_drifttime= 12.1863374710083
mean_tot= 26.70804214477539; std_tot= 13.273002624511719
mean_t0= 13.745773315429688; std_t0= 3.476813316345215
mean_hit_l= 3.492910861968994; std_hit_l= 1.2237294912338257
mean_hit_bec= -0.0016692180919606222; std_hit_bec= 1.5962193862422314
mean_hit_layer= 3.1971582916069843; std_hit_layer= 3.4222705930841375
mean_hit_strawlayer= 8.967285048779083; std_hit_strawlayer= 6.76512181789521
mean_hit_strawnumber= 11.07296157203907; std_hit_strawnumber= 6.8633168930581885
mean_hit_localTheta= 1.5726525783538818; std_hit_localTheta= 0.8492729663848877
mean_hit_localPhi= 0.01586398482322693; std_hit_localPhi= 1.8093135356903076
mean_hit_HitZ= -4.6137542724609375; std_hit_HitZ= 1265.109375
mean_hit_HitR= 808.4691162109375; std_hit_HitR= 119.3169174194336
mean_hit_rTrkWire= 1.0269434452056885; std_hit_rTrkWire= 0.6175986528396606
mean_hit_ZR= 585.8178100585938; std_hit_ZR= 272.51654052734375

#Track variables
mean_p = 56474.36265135193; std_p = 39000.085012894924
mean_trkOcc = 0.4023797888947447; std_trkOcc = 0.19136651740017593
mean_fHTMB = 31.2387109375; std_fHTMB = 6.70491726465121
mean_nTRThits = 31.2387109375; std_nTRThits = 6.70491726465121
mean_eta = -1.2681346502045443e-05; std_eta = 1.1016868661920247
mean_phi = 0.005611623766507478; std_phi = 1.8122392759016057
mean_avgmu = 29.858097706481814; std_avgmu = 18.006054125066736
mean_lepPT = 36580.7761164999; std_lepPT = 16874.367172679427

features = ["avgmu", "trkOcc", "p", "lep_pT", "eta", "phi", "nTRThits", "fHTMB", "eProbHT", "hit_HTMB", 
                "hit_gasType", "hit_drifttime", "hit_tot", "hit_T0", "hit_L",
                "hit_bec", "hit_rTrkWire", "hit_strawlayer",
                "hit_bec", "hit_layer", "hit_strawlayer", "hit_strawnumber",
                "hit_localTheta", "hit_localPhi", "hit_HitZ", "hit_HitR",
                "hit_rTrkWire", "hit_ZR"]

def trackGenerator(filelist, particle_type = "electron"): 
    label = 1 if particle_type == "electron" else 0
    for k in filelist[particle_type]:
        for arrays in uproot.iterate(k, particle_type + "_mc", features): 
            for i in range(len(arrays[b'p'])): #loop over all the tracks in the root file
                e_eProbHT = arrays[b'eProbHT'][i]
                hit = []
                track = []
                if(arrays[b'hit_HTMB'][i][-1] == 99999): #check the hit arrays are padded with '99999' at the end
                    hit_length = list(arrays[b'hit_HTMB'][i]).index(99999)
                else:
                    hit_length = len(arrays[b'hit_HTMB'][i])
                for j in range(hit_length):
                    hit.append([(arrays[b'hit_HTMB'][i][j]- mean_HTMB)/std_HTMB,
                                (arrays[b'hit_gasType'][i][j]- mean_gasType)/std_gasType,
                                (arrays[b'hit_drifttime'][i][j]- mean_drifttime)/std_drifttime,
                                (arrays[b'hit_tot'][i][j]- mean_tot)/std_tot,
                                (arrays[b'hit_T0'][i][j]- mean_t0)/std_t0,
                                (arrays[b'hit_L'][i][j]- mean_hit_l)/std_hit_l,
                                (arrays[b'hit_bec'][i][j]- mean_hit_bec)/std_hit_bec ,
                                (arrays[b'hit_layer'][i][j]- mean_hit_layer)/std_hit_layer,
                                (arrays[b'hit_strawlayer'][i][j]- mean_hit_strawlayer)/std_hit_strawlayer,
                                (arrays[b'hit_strawnumber'][i][j]- mean_hit_strawnumber)/std_hit_strawnumber,
                                (arrays[b'hit_localTheta'][i][j]- mean_hit_localTheta)/std_hit_localTheta,
                                (arrays[b'hit_localPhi'][i][j]- mean_hit_localPhi)/std_hit_localPhi,
                                (arrays[b'hit_HitZ'][i][j]- mean_hit_HitZ)/std_hit_HitZ,
                                (arrays[b'hit_HitR'][i][j]- mean_hit_HitR)/std_hit_HitR,
                                (arrays[b'hit_rTrkWire'][i][j]- mean_hit_rTrkWire)/std_hit_rTrkWire,
                                (arrays[b'hit_ZR'][i][j]- mean_hit_ZR)/std_hit_ZR ])
                yield [hit, [(arrays[b'avgmu'][i] - mean_avgmu)/std_avgmu, (arrays[b'lep_pT'][i] - mean_lepPT)/std_lepPT,
                             (arrays[b'phi'][i] - mean_phi)/std_phi, (arrays[b'p'][i] - mean_p)/std_p, 
                             (arrays[b'eta'][i] - mean_eta)/std_eta,
                             (arrays[b'fHTMB'][i] - mean_fHTMB)/std_fHTMB, (arrays[b'trkOcc'][i] - mean_trkOcc)/std_trkOcc, 
                             (arrays[b'nTRThits'][i] - mean_nTRThits)/std_nTRThits], e_eProbHT, label]
